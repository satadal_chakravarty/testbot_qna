
// Copyright (c) Microsoft Corporation. All rights reserved.
// Licensed under the MIT License.

import { ActionTypes, ActivityHandler, CardFactory, MessageFactory } from 'botbuilder';
import { QnAMaker } from 'botbuilder-ai' ;
import { IQnAClientStub } from './qna/interfaces/IQnAClientFacade';
import { QnAResponseModel } from './qna/models/QnAResponseModel';
import { QnAStubFactory } from './qna/QnAStubFactory';
import {
    Activity,
    ActivityTypes,
    BotState,
    ChannelAccount,
    ConversationState,
    Mention,
    StatePropertyAccessor,
    TurnContext,
    UserState,
     
  } from 'botbuilder';

  import { Dialog, DialogState } from 'botbuilder-dialogs';
  import { MainDialog } from './dialogs/mainDialog';
import { ConversationStateData } from './dialogs/siteDetails';

export class EchoBot extends ActivityHandler {
    private convstateaccessor: StatePropertyAccessor<ConversationStateData>;

    // This line determines what the keys and values of our object can be.
    // In this case the keys can be string while the values can be anything.
    [x: string]: any;
    private qnaStubFactory: QnAStubFactory;
    
    constructor( configStr: string ,
        private conversationState: BotState,
        private userState: BotState,
        ) {
        
        super();
        //console.log(configStr);
        
        this.convstateaccessor = this.conversationState.createProperty<ConversationStateData>('siteDetails');

        this.qnaStubFactory = QnAStubFactory.getInstance(true, configStr);     
             
        this.dialog = new MainDialog('mainDialog',configStr ,this.convstateaccessor);

        this.onMessage(async (context, next) => {  
           
            const user_query =  context.activity.text;
            var _this = this ;
            
            const convstatedata = await this.convstateaccessor.get(context,  {domain : null} as ConversationStateData )
            //console.log(convstatedata);

            await (this.dialog as MainDialog).run(context ,configStr , this.convstateaccessor);
          
            await next();
        });
        
     this.onDialog(async (context, next) => {
                // Save any state changes. The load happened during the execution of the Dialog.
                await this.conversationState.saveChanges(context, false);
                await this.userState.saveChanges(context, false);
          
                // By calling next() you ensure that the next BotHandler is run.
                await next();
     });
       
        this.onMembersAdded(async (context, next) => {
            const membersAdded = context.activity.membersAdded;
            const welcomeText = "Hello and welcome to People's Taiho Buddy bot..." ;
            for (const member of membersAdded) {
                if (member.id !== context.activity.recipient.id) {
                    await context.sendActivity(MessageFactory.text(welcomeText, welcomeText));
                    
                }
            }
            // By calling next() you ensure that the next BotHandler is run.
            await next();
        });
    }

   
    public async run(context): Promise<void> {
        await super.run(context);
    
        // Save any state changes. The load happened during the execution of the Dialog.
        await this.conversationState.saveChanges(context, false);
        await this.userState.saveChanges(context, false);
      }
}





//async function sendButtons(turncontext){
                
    //     const cardActions = [
    //     {
    //         type: ActionTypes.PostBack,
    //         title: 'HR - Department',
    //         value: 'HR',
    //         //image: '',
    //         imageAltText: 'H'
    //     },
    //     {
    //         type: ActionTypes.PostBack,
    //         title: 'Flight - Help',
    //         value: 'Flight',
    //         //image: '',
    //         imageAltText: 'F'
    //     }
    // ] ;

    // var reply = MessageFactory.suggestedActions(cardActions, 'Which department do you want to connect to ?');
    // return reply;
    

    //     }













