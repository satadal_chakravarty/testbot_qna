import { TurnContext } from "botbuilder-core";
import { IQnAClientStub } from "./interfaces/IQnAClientFacade";
import { IQnAConfigLoader } from "./interfaces/IQnAConfigLoader";
import { QnAConfig } from "./models/QnAConfig";
import { QnAClientStub } from "./QnAClientStub";
import { DBBasedConfigLoader, LocalConfigLoader } from "./QnAConfigLoaders";

export class QnAStubFactory {

    domainQnAInstances: Map<string, IQnAClientStub> = new Map();
    static _instance: QnAStubFactory = null;

    private constructor(private useLocal: boolean, private configStr: string) {
        this.loadAllDomainQnAInstances();
    }
    
    public static getInstance(useLocal: boolean, configStr?: string): QnAStubFactory {
        if (this._instance === null) {
            this._instance = new QnAStubFactory(useLocal, configStr);
        }
        return this._instance;
    }

    public getQnAStub(domain: string): IQnAClientStub {
        if (this.domainQnAInstances !== null && this.domainQnAInstances.has(domain))
            return this.domainQnAInstances.get(domain);
        return null;    
    }

    private loadAllDomainQnAInstances() {
        const configLoader: IQnAConfigLoader = this.getConfigLoader(this.useLocal);
        const configs: QnAConfig[] = configLoader.loadConfigurations();
        this.instantiateMakers(configs);
    }

    private instantiateMakers(configs: QnAConfig[]) {
        configs.forEach( a => this.domainQnAInstances.set(a.domain, new QnAClientStub(a)));
    }

    private getConfigLoader(useLocal: boolean): IQnAConfigLoader {
        if (useLocal)
            return new LocalConfigLoader(this.configStr);
        return new DBBasedConfigLoader();    
    }


}

