import { QnAMaker, QnAMakerResult } from "botbuilder-ai";
import { TurnContext } from "botbuilder-core";
import { IQnAClientStub } from "./interfaces/IQnAClientFacade";
import { QnAConfig } from "./models/QnAConfig";
import { QnAResponseModel } from "./models/QnAResponseModel";

export class QnAClientStub implements IQnAClientStub {

    qnaMaker: QnAMaker;
    
    constructor(config: QnAConfig) {
        this.qnaMaker = new QnAMaker(config, {top: config.maxresponses});
    }

    async invoke(ctx: TurnContext): Promise<QnAResponseModel[]> {
        const responses: QnAMakerResult[] = await this.qnaMaker.getAnswers(ctx);
        return responses.map( a => {
            const r = {confidence: a.score, source: a.source, text: a.answer} as QnAResponseModel;
            return r;
        })
    }

} 