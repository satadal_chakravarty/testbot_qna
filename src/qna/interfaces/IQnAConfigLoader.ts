import { QnAConfig } from "../models/QnAConfig";

export interface IQnAConfigLoader {
    loadConfigurations(): QnAConfig[];
}