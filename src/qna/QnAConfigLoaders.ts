import { IQnAConfigLoader } from "./interfaces/IQnAConfigLoader";
import { QnAConfig } from "./models/QnAConfig";

export class DBBasedConfigLoader implements IQnAConfigLoader {
    
    loadConfigurations(): QnAConfig[] {
        throw new Error("Method not implemented.");
    }

}

export class LocalConfigLoader implements IQnAConfigLoader {
    
    constructor(private configStr: string) {
    }


    loadConfigurations(): QnAConfig[] {
        if (this.configStr)
            return JSON.parse(this.configStr) as QnAConfig[];
        return [];
    }

}