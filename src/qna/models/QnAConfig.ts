export interface QnAConfig {
    domain: string;
    maxresponses: number;
    knowledgeBaseId: string;
    endpointKey: string;
    host: string;
}