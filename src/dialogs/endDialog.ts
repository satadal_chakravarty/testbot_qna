import {
    ChoiceFactory,
    ChoicePrompt,
    ComponentDialog,
    ConfirmPrompt,
    DialogTurnResult,
    TextPrompt,
    WaterfallDialog,
    WaterfallStepContext
} from 'botbuilder-dialogs';

const WATERFALL_DIALOG = 'waterfallDialog';

export class EndDialog extends ComponentDialog {
    
    constructor(id: string ) {
        super(id || 'endDialog');   
        
        this.addDialog(new WaterfallDialog(WATERFALL_DIALOG, [
            this.endDialog_step.bind(this) ]));

            this.initialDialogId = WATERFALL_DIALOG;    
    }

    private async endDialog_step(stepContext: WaterfallStepContext) {
       await stepContext.cancelAllDialogs();
       return await stepContext.context.sendActivity("Thank you so much for your time.");
    }
    
}