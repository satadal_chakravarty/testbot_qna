
import { ActionTypes, MessageFactory, StatePropertyAccessor, TurnContext, UserState } from 'botbuilder';
import {
    ChoiceFactory,
    ChoicePrompt,
    ComponentDialog,
    ConfirmPrompt,
    DialogSet,
    DialogTurnStatus,
    DialogTurnResult,
    NumberPrompt,
    PromptValidatorContext,
    TextPrompt,
    WaterfallDialog,
    WaterfallStepContext
} from 'botbuilder-dialogs';
import { ConversationStateData } from './siteDetails';
import { SiteDialog } from './siteDialog';
import { EndDialog } from './endDialog';


const SITE_DIALOG = 'siteDialog';
const END_DIALOG = 'endDialog';
const WATERFALL_DIALOG = 'waterfallDialog';
const CHOICE_PROMPT = 'choicePrompt';

export class MainDialog extends ComponentDialog {
    constructor(id: string ,  configStr: string , private convstateaccessor :StatePropertyAccessor<ConversationStateData>) {
        super(id);
        this.addDialog(new SiteDialog(SITE_DIALOG , configStr , this.convstateaccessor ))
        this.addDialog(new ChoicePrompt(CHOICE_PROMPT))
        this.addDialog(new EndDialog(END_DIALOG))
        this.addDialog(new WaterfallDialog(WATERFALL_DIALOG, [this.Question_step.bind(this) , this.Fetch_Step.bind(this)] ));
          
        this.initialDialogId = WATERFALL_DIALOG;
    }    

    public async run(turnContext: TurnContext,  configStr : string , convstateaccessor: StatePropertyAccessor ) {
        const dialogSet = new DialogSet(convstateaccessor);
        dialogSet.add(this);
        const dialogContext = await dialogSet.createContext(turnContext);        
        if(turnContext.activity.text.toLowerCase().toString() == 'change department')
        {
            const siteDetails = await this.convstateaccessor.get(turnContext) ;
            siteDetails.domain = null ;
            siteDetails.current_question = null ;
            dialogContext.cancelAllDialogs();
        }

        const results = await dialogContext.continueDialog();
        if (results.status === DialogTurnStatus.empty) {
            await dialogContext.beginDialog(this.id );            
        }
    }

    private async Question_step(stepContext: WaterfallStepContext):Promise<DialogTurnResult> {       
        const siteDetails = await this.convstateaccessor.get(stepContext.context) ;
        console.log(siteDetails.domain);
        return await stepContext.beginDialog(SITE_DIALOG);      
    }

    private async Fetch_Step(stepContext: WaterfallStepContext) {

        await stepContext.context.sendActivity('I hope I was helpful.');
        await stepContext.prompt(CHOICE_PROMPT, {
            choices: ChoiceFactory.toChoices(['Another question', 'Change Department' ,'Contact Department', 'Feedback']),
            prompt: 'What would you like to do next ?'
        });
        const siteDetails = await this.convstateaccessor.get(stepContext.context) ;
        console.log(siteDetails.domain);
        return await stepContext.endDialog();        

    }
        
}

