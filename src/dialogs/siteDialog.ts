import { ActionTypes, MessageFactory, StatePropertyAccessor, TurnContext, UserState } from 'botbuilder';

import {
    ChoiceFactory,
    ChoicePrompt,
    ComponentDialog,
    ConfirmPrompt,
    DialogTurnResult,
    TextPrompt,
    WaterfallDialog,
    WaterfallStepContext
} from 'botbuilder-dialogs';
import { config } from 'dotenv/types';
import { IQnAClientStub } from '../qna/interfaces/IQnAClientFacade';
import { QnAResponseModel } from '../qna/models/QnAResponseModel';
import { QnAStubFactory } from '../qna/QnAStubFactory';

import { ConversationStateData } from './siteDetails';

const TEXT_PROMPT = 'textPrompt';
const CHOICE_PROMPT = 'choicePrompt';
const WATERFALL_DIALOG = 'waterfallDialog';

export class SiteDialog extends ComponentDialog {

    private qnaStubFactory: QnAStubFactory;
    constructor(id: string ,  configStr : string , private convstateaccessor :StatePropertyAccessor<ConversationStateData>) {
        super(id || 'siteDialog');       
        
        
        this.qnaStubFactory = QnAStubFactory.getInstance(true, configStr);
        
        this
            .addDialog(new TextPrompt(TEXT_PROMPT))
            .addDialog(new ChoicePrompt(CHOICE_PROMPT))
            .addDialog(new WaterfallDialog(WATERFALL_DIALOG, [
                this.take_domain_step.bind(this),
                this.take_query_step.bind(this),
                this.finalStep.bind(this),
                
            ]));
        this.initialDialogId = WATERFALL_DIALOG;

    }    

    private async take_domain_step(stepContext: WaterfallStepContext): Promise<DialogTurnResult> {
        const siteDetails = await this.convstateaccessor.get(stepContext.context) ;
        
        if(siteDetails.domain === null){
            return await stepContext.prompt(CHOICE_PROMPT, {
                choices: ChoiceFactory.toChoices(['People', 'IT Services']),
                prompt: 'Please select the department you want to connect to - '
            });
        }
        else
        {
            return await stepContext.next(siteDetails.domain)
        }
    }
    
    private async take_query_step(stepContext: WaterfallStepContext): Promise<DialogTurnResult> {
        const siteDetails = await this.convstateaccessor.get(stepContext.context) ;        
        if(stepContext.result){
                siteDetails.domain = stepContext.result.value || stepContext.result ; 
                
        }
        const promptText = 'Please provide your query :';
        return await stepContext.prompt(TEXT_PROMPT, { prompt: promptText });          
    }
    
    
    private async finalStep(stepContext: WaterfallStepContext){
        const siteDetails = await this.convstateaccessor.get(stepContext.context) ;
        if(stepContext.result){
            siteDetails.current_question = stepContext.result ; 
        }
        const qnaStub: IQnAClientStub = this.qnaStubFactory.getQnAStub(siteDetails.domain);       
        const responseModel: QnAResponseModel[] = await qnaStub.invoke(stepContext.context);
        
        if (responseModel.length == 1) {
                    await stepContext.context.sendActivity(`I got the following result : ' ${ responseModel[0].text}`);
                }
                else if(responseModel.length > 1){
                    await stepContext.context.sendActivity(`I got the following results : `);
                    for(let i=0 ; i<responseModel.length ; i++){
                        await stepContext.context.sendActivity(responseModel[i].text)
                    }
                    
                }
                else {
                    // If no answers were returned from QnA Maker, reply with help.
                    await stepContext.context.sendActivity('No QnA Maker response was returned.');
                }
     return await stepContext.endDialog();
        
    }

    

}
