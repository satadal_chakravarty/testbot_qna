import { StatePropertyAccessor } from 'botbuilder';
import { ComponentDialog } from 'botbuilder-dialogs';
import { ConversationStateData } from './siteDetails';
export declare class SiteDialog extends ComponentDialog {
    private convstateaccessor;
    private qnaStubFactory;
    constructor(id: string, configStr: string, convstateaccessor: StatePropertyAccessor<ConversationStateData>);
    private take_domain_step;
    private take_query_step;
    private finalStep;
}
