"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.MainDialog = void 0;
const botbuilder_dialogs_1 = require("botbuilder-dialogs");
const siteDialog_1 = require("./siteDialog");
const endDialog_1 = require("./endDialog");
const SITE_DIALOG = 'siteDialog';
const END_DIALOG = 'endDialog';
const WATERFALL_DIALOG = 'waterfallDialog';
const CHOICE_PROMPT = 'choicePrompt';
class MainDialog extends botbuilder_dialogs_1.ComponentDialog {
    constructor(id, configStr, convstateaccessor) {
        super(id);
        this.convstateaccessor = convstateaccessor;
        this.addDialog(new siteDialog_1.SiteDialog(SITE_DIALOG, configStr, this.convstateaccessor));
        this.addDialog(new botbuilder_dialogs_1.ChoicePrompt(CHOICE_PROMPT));
        this.addDialog(new endDialog_1.EndDialog(END_DIALOG));
        this.addDialog(new botbuilder_dialogs_1.WaterfallDialog(WATERFALL_DIALOG, [this.Question_step.bind(this), this.Fetch_Step.bind(this)]));
        this.initialDialogId = WATERFALL_DIALOG;
    }
    run(turnContext, configStr, convstateaccessor) {
        return __awaiter(this, void 0, void 0, function* () {
            const dialogSet = new botbuilder_dialogs_1.DialogSet(convstateaccessor);
            dialogSet.add(this);
            const dialogContext = yield dialogSet.createContext(turnContext);
            if (turnContext.activity.text.toLowerCase().toString() == 'change department') {
                const siteDetails = yield this.convstateaccessor.get(turnContext);
                siteDetails.domain = null;
                siteDetails.current_question = null;
                dialogContext.cancelAllDialogs();
            }
            const results = yield dialogContext.continueDialog();
            if (results.status === botbuilder_dialogs_1.DialogTurnStatus.empty) {
                yield dialogContext.beginDialog(this.id);
            }
        });
    }
    Question_step(stepContext) {
        return __awaiter(this, void 0, void 0, function* () {
            const siteDetails = yield this.convstateaccessor.get(stepContext.context);
            console.log(siteDetails.domain);
            return yield stepContext.beginDialog(SITE_DIALOG);
        });
    }
    Fetch_Step(stepContext) {
        return __awaiter(this, void 0, void 0, function* () {
            yield stepContext.context.sendActivity('I hope I was helpful.');
            yield stepContext.prompt(CHOICE_PROMPT, {
                choices: botbuilder_dialogs_1.ChoiceFactory.toChoices(['Another question', 'Change Department', 'Contact Department', 'Feedback']),
                prompt: 'What would you like to do next ?'
            });
            const siteDetails = yield this.convstateaccessor.get(stepContext.context);
            console.log(siteDetails.domain);
            return yield stepContext.endDialog();
        });
    }
}
exports.MainDialog = MainDialog;
//# sourceMappingURL=mainDialog.js.map