"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.EndDialog = void 0;
const botbuilder_dialogs_1 = require("botbuilder-dialogs");
const WATERFALL_DIALOG = 'waterfallDialog';
class EndDialog extends botbuilder_dialogs_1.ComponentDialog {
    constructor(id) {
        super(id || 'endDialog');
        this.addDialog(new botbuilder_dialogs_1.WaterfallDialog(WATERFALL_DIALOG, [
            this.endDialog_step.bind(this)
        ]));
        this.initialDialogId = WATERFALL_DIALOG;
    }
    endDialog_step(stepContext) {
        return __awaiter(this, void 0, void 0, function* () {
            yield stepContext.cancelAllDialogs();
            return yield stepContext.context.sendActivity("Thank you so much for your time.");
        });
    }
}
exports.EndDialog = EndDialog;
//# sourceMappingURL=endDialog.js.map