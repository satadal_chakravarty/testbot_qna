import { StatePropertyAccessor, TurnContext } from 'botbuilder';
import { ComponentDialog } from 'botbuilder-dialogs';
import { ConversationStateData } from './siteDetails';
export declare class MainDialog extends ComponentDialog {
    private convstateaccessor;
    constructor(id: string, configStr: string, convstateaccessor: StatePropertyAccessor<ConversationStateData>);
    run(turnContext: TurnContext, configStr: string, convstateaccessor: StatePropertyAccessor): Promise<void>;
    private Question_step;
    private Fetch_Step;
}
