"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.SiteDialog = void 0;
const botbuilder_dialogs_1 = require("botbuilder-dialogs");
const QnAStubFactory_1 = require("../qna/QnAStubFactory");
const TEXT_PROMPT = 'textPrompt';
const CHOICE_PROMPT = 'choicePrompt';
const WATERFALL_DIALOG = 'waterfallDialog';
class SiteDialog extends botbuilder_dialogs_1.ComponentDialog {
    constructor(id, configStr, convstateaccessor) {
        super(id || 'siteDialog');
        this.convstateaccessor = convstateaccessor;
        this.qnaStubFactory = QnAStubFactory_1.QnAStubFactory.getInstance(true, configStr);
        this
            .addDialog(new botbuilder_dialogs_1.TextPrompt(TEXT_PROMPT))
            .addDialog(new botbuilder_dialogs_1.ChoicePrompt(CHOICE_PROMPT))
            .addDialog(new botbuilder_dialogs_1.WaterfallDialog(WATERFALL_DIALOG, [
            this.take_domain_step.bind(this),
            this.take_query_step.bind(this),
            this.finalStep.bind(this),
        ]));
        this.initialDialogId = WATERFALL_DIALOG;
    }
    take_domain_step(stepContext) {
        return __awaiter(this, void 0, void 0, function* () {
            const siteDetails = yield this.convstateaccessor.get(stepContext.context);
            if (siteDetails.domain === null) {
                return yield stepContext.prompt(CHOICE_PROMPT, {
                    choices: botbuilder_dialogs_1.ChoiceFactory.toChoices(['People', 'IT Services']),
                    prompt: 'Please select the department you want to connect to - '
                });
            }
            else {
                return yield stepContext.next(siteDetails.domain);
            }
        });
    }
    take_query_step(stepContext) {
        return __awaiter(this, void 0, void 0, function* () {
            const siteDetails = yield this.convstateaccessor.get(stepContext.context);
            if (stepContext.result) {
                siteDetails.domain = stepContext.result.value || stepContext.result;
            }
            const promptText = 'Please provide your query :';
            return yield stepContext.prompt(TEXT_PROMPT, { prompt: promptText });
        });
    }
    finalStep(stepContext) {
        return __awaiter(this, void 0, void 0, function* () {
            const siteDetails = yield this.convstateaccessor.get(stepContext.context);
            if (stepContext.result) {
                siteDetails.current_question = stepContext.result;
            }
            const qnaStub = this.qnaStubFactory.getQnAStub(siteDetails.domain);
            const responseModel = yield qnaStub.invoke(stepContext.context);
            if (responseModel.length == 1) {
                yield stepContext.context.sendActivity(`I got the following result : ' ${responseModel[0].text}`);
            }
            else if (responseModel.length > 1) {
                yield stepContext.context.sendActivity(`I got the following results : `);
                for (let i = 0; i < responseModel.length; i++) {
                    yield stepContext.context.sendActivity(responseModel[i].text);
                }
            }
            else {
                // If no answers were returned from QnA Maker, reply with help.
                yield stepContext.context.sendActivity('No QnA Maker response was returned.');
            }
            return yield stepContext.endDialog();
        });
    }
}
exports.SiteDialog = SiteDialog;
//# sourceMappingURL=siteDialog.js.map