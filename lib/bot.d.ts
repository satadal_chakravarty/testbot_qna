import { ActivityHandler } from 'botbuilder';
import { BotState } from 'botbuilder';
export declare class EchoBot extends ActivityHandler {
    private conversationState;
    private userState;
    private convstateaccessor;
    [x: string]: any;
    private qnaStubFactory;
    constructor(configStr: string, conversationState: BotState, userState: BotState);
    run(context: any): Promise<void>;
}
