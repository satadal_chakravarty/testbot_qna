"use strict";
// Copyright (c) Microsoft Corporation. All rights reserved.
// Licensed under the MIT License.
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.EchoBot = void 0;
const botbuilder_1 = require("botbuilder");
const QnAStubFactory_1 = require("./qna/QnAStubFactory");
const mainDialog_1 = require("./dialogs/mainDialog");
class EchoBot extends botbuilder_1.ActivityHandler {
    constructor(configStr, conversationState, userState) {
        super();
        this.conversationState = conversationState;
        this.userState = userState;
        //console.log(configStr);
        this.convstateaccessor = this.conversationState.createProperty('siteDetails');
        this.qnaStubFactory = QnAStubFactory_1.QnAStubFactory.getInstance(true, configStr);
        this.dialog = new mainDialog_1.MainDialog('mainDialog', configStr, this.convstateaccessor);
        this.onMessage((context, next) => __awaiter(this, void 0, void 0, function* () {
            const user_query = context.activity.text;
            var _this = this;
            const convstatedata = yield this.convstateaccessor.get(context, { domain: null });
            //console.log(convstatedata);
            yield this.dialog.run(context, configStr, this.convstateaccessor);
            yield next();
        }));
        this.onDialog((context, next) => __awaiter(this, void 0, void 0, function* () {
            // Save any state changes. The load happened during the execution of the Dialog.
            yield this.conversationState.saveChanges(context, false);
            yield this.userState.saveChanges(context, false);
            // By calling next() you ensure that the next BotHandler is run.
            yield next();
        }));
        this.onMembersAdded((context, next) => __awaiter(this, void 0, void 0, function* () {
            const membersAdded = context.activity.membersAdded;
            const welcomeText = "Hello and welcome to People's Taiho Buddy bot...";
            for (const member of membersAdded) {
                if (member.id !== context.activity.recipient.id) {
                    yield context.sendActivity(botbuilder_1.MessageFactory.text(welcomeText, welcomeText));
                }
            }
            // By calling next() you ensure that the next BotHandler is run.
            yield next();
        }));
    }
    run(context) {
        const _super = Object.create(null, {
            run: { get: () => super.run }
        });
        return __awaiter(this, void 0, void 0, function* () {
            yield _super.run.call(this, context);
            // Save any state changes. The load happened during the execution of the Dialog.
            yield this.conversationState.saveChanges(context, false);
            yield this.userState.saveChanges(context, false);
        });
    }
}
exports.EchoBot = EchoBot;
//async function sendButtons(turncontext){
//     const cardActions = [
//     {
//         type: ActionTypes.PostBack,
//         title: 'HR - Department',
//         value: 'HR',
//         //image: '',
//         imageAltText: 'H'
//     },
//     {
//         type: ActionTypes.PostBack,
//         title: 'Flight - Help',
//         value: 'Flight',
//         //image: '',
//         imageAltText: 'F'
//     }
// ] ;
// var reply = MessageFactory.suggestedActions(cardActions, 'Which department do you want to connect to ?');
// return reply;
//     }
//# sourceMappingURL=bot.js.map