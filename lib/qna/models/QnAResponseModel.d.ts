export interface QnAResponseModel {
    text: string;
    confidence: number;
    source: string;
}
