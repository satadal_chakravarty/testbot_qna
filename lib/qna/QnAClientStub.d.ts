import { QnAMaker } from "botbuilder-ai";
import { TurnContext } from "botbuilder-core";
import { IQnAClientStub } from "./interfaces/IQnAClientFacade";
import { QnAConfig } from "./models/QnAConfig";
import { QnAResponseModel } from "./models/QnAResponseModel";
export declare class QnAClientStub implements IQnAClientStub {
    qnaMaker: QnAMaker;
    constructor(config: QnAConfig);
    invoke(ctx: TurnContext): Promise<QnAResponseModel[]>;
}
