import { TurnContext } from "botbuilder-core";
import { QnAResponseModel } from "../models/QnAResponseModel";
export interface IQnAClientStub {
    invoke(ctx: TurnContext): Promise<QnAResponseModel[]>;
}
