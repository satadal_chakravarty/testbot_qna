"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.QnAStubFactory = void 0;
const QnAClientStub_1 = require("./QnAClientStub");
const QnAConfigLoaders_1 = require("./QnAConfigLoaders");
class QnAStubFactory {
    constructor(useLocal, configStr) {
        this.useLocal = useLocal;
        this.configStr = configStr;
        this.domainQnAInstances = new Map();
        this.loadAllDomainQnAInstances();
    }
    static getInstance(useLocal, configStr) {
        if (this._instance === null) {
            this._instance = new QnAStubFactory(useLocal, configStr);
        }
        return this._instance;
    }
    getQnAStub(domain) {
        if (this.domainQnAInstances !== null && this.domainQnAInstances.has(domain))
            return this.domainQnAInstances.get(domain);
        return null;
    }
    loadAllDomainQnAInstances() {
        const configLoader = this.getConfigLoader(this.useLocal);
        const configs = configLoader.loadConfigurations();
        this.instantiateMakers(configs);
    }
    instantiateMakers(configs) {
        configs.forEach(a => this.domainQnAInstances.set(a.domain, new QnAClientStub_1.QnAClientStub(a)));
    }
    getConfigLoader(useLocal) {
        if (useLocal)
            return new QnAConfigLoaders_1.LocalConfigLoader(this.configStr);
        return new QnAConfigLoaders_1.DBBasedConfigLoader();
    }
}
exports.QnAStubFactory = QnAStubFactory;
QnAStubFactory._instance = null;
//# sourceMappingURL=QnAStubFactory.js.map