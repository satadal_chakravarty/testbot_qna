import { IQnAClientStub } from "./interfaces/IQnAClientFacade";
export declare class QnAStubFactory {
    private useLocal;
    private configStr;
    domainQnAInstances: Map<string, IQnAClientStub>;
    static _instance: QnAStubFactory;
    private constructor();
    static getInstance(useLocal: boolean, configStr?: string): QnAStubFactory;
    getQnAStub(domain: string): IQnAClientStub;
    private loadAllDomainQnAInstances;
    private instantiateMakers;
    private getConfigLoader;
}
