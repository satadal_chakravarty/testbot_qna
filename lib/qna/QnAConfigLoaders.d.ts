import { IQnAConfigLoader } from "./interfaces/IQnAConfigLoader";
import { QnAConfig } from "./models/QnAConfig";
export declare class DBBasedConfigLoader implements IQnAConfigLoader {
    loadConfigurations(): QnAConfig[];
}
export declare class LocalConfigLoader implements IQnAConfigLoader {
    private configStr;
    constructor(configStr: string);
    loadConfigurations(): QnAConfig[];
}
