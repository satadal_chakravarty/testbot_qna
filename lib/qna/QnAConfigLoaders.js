"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.LocalConfigLoader = exports.DBBasedConfigLoader = void 0;
class DBBasedConfigLoader {
    loadConfigurations() {
        throw new Error("Method not implemented.");
    }
}
exports.DBBasedConfigLoader = DBBasedConfigLoader;
class LocalConfigLoader {
    constructor(configStr) {
        this.configStr = configStr;
    }
    loadConfigurations() {
        if (this.configStr)
            return JSON.parse(this.configStr);
        return [];
    }
}
exports.LocalConfigLoader = LocalConfigLoader;
//# sourceMappingURL=QnAConfigLoaders.js.map