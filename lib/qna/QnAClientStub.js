"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.QnAClientStub = void 0;
const botbuilder_ai_1 = require("botbuilder-ai");
class QnAClientStub {
    constructor(config) {
        this.qnaMaker = new botbuilder_ai_1.QnAMaker(config, { top: config.maxresponses });
    }
    invoke(ctx) {
        return __awaiter(this, void 0, void 0, function* () {
            const responses = yield this.qnaMaker.getAnswers(ctx);
            return responses.map(a => {
                const r = { confidence: a.score, source: a.source, text: a.answer };
                return r;
            });
        });
    }
}
exports.QnAClientStub = QnAClientStub;
//# sourceMappingURL=QnAClientStub.js.map